document.addEventListener('DOMContentLoaded', () => {
    const tabTitles = document.querySelectorAll('.tabs-title');
    const tabContents = document.querySelectorAll('.tabs-content li');

    // - vmist
    tabContents.forEach((content) => {
        content.style.display = 'none';
    });

    tabTitles.forEach((title, index) => {
        title.addEventListener('click', (event) => {
            event.preventDefault();

            // -vmist
            tabContents.forEach((content) => {
                content.style.display = 'none';
            });

            tabTitles.forEach((t) => t.classList.remove('active'));

            // +vmist
            tabContents[index].style.display = 'block';
            title.classList.add('active');
        });
    });
});


document.addEventListener('DOMContentLoaded', () => {
    const tabsContainer = document.getElementById('tabs');

    tabsContainer.addEventListener('click', (event) => {
        const target = event.target;

        // e click/nema click
        if (target.classList.contains('tabs-title')) {
            const tabTitles = tabsContainer.querySelectorAll('.tabs-title');
            const tabContents = document.querySelectorAll('.tabs-content .tab-content-item');

            // -tabs
            tabContents.forEach(content => {
                content.style.display = 'none';
            });

            // -active
            tabTitles.forEach(title => {
                title.classList.remove('active');
            });

            // index of tab
            const index = Array.from(tabTitles).indexOf(target);

            // content of tab
            tabContents[index].style.display = 'block';

            // +active
            target.classList.add('active');
        }
    });
});
